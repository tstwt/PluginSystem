﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TDesktop
{
    internal class _Startup
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            System.Windows.Forms.Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            try
            {
                Application.ActiveApplication = new Application();
                Application.ActiveApplication.Args = args;
                ShowSplashForm();
                Application.ActiveApplication.Initialize();
                Application.ActiveApplication.SplashForm.Close();//启动界面

                Application.ActiveApplication.Run();

                Application.ActiveApplication.Exit();
            }
            catch (Exception ex)
            {
                if (Application.ActiveApplication != null
                    && Application.ActiveApplication.Output != null)//也有可能是在Application构造的时候抛的异常，这时需要判断一下，以免再次抛异常导致卡死。
                {
                    Application.ActiveApplication.SplashForm.Close();//启动界面
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Information);
                }
            }

        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            //if (Application.ActiveApplication != null &&
            //    Application.ActiveApplication.Output != null)
            //{

            //}
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //if (Application.ActiveApplication != null &&
            //   Application.ActiveApplication.Output != null)
            //{
            //    Application.ActiveApplication.Output.Output(((Exception)e.ExceptionObject).StackTrace, InfoType.Exception);
            //}
        }

        static void ShowSplashForm()
        {
            TDesktop.Application.ActiveApplication.SplashForm.Show();
        }
    }
}
