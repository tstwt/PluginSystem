﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop.PluginManager
{
    public class PluginManager : TPlugin
    {
        public override TDockBar Initialize()
        {
            TDockBar dock = new TDockBar();
            dock.Caption = "插件管理器";
            dock.Control = new CtrlPluginManager();
            return dock;
        }
    }
}
