﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace TDesktop.PluginManager
{
    public partial class CtrlPluginManager : XtraUserControl
    {
        private DataTable dt;
        private string PluginPath = Application.PluginDirPath;
        List<string> DeleteList = new List<string>();

        public CtrlPluginManager()
        {
            InitializeComponent();
        }

        private void CtrlPluginManager_Load(object sender, System.EventArgs e)
        {
            initDatatable();
            this.gridView1.BestFitColumns();
            Application.ActiveApplication.MainForm.MainClosed += (o, args) =>
            {
                //if (DeleteList.Count != 0)
                //{
                //    foreach (string s in DeleteList)
                //    {
                //        File.Delete(s);
                //    }
                //}
            };
        }

        private void initDatatable()
        {
            dt = new DataTable();
            dt.Columns.Add("名称");
            dt.Columns.Add("版本");
            dt.Columns.Add("地址");
            gridControl1.DataSource = dt;

            InitPlugin();

            progressBar1.Properties.Minimum = 0;
            progressBar1.Properties.Maximum = 100;
        }

        private void InitPlugin()
        {
            dt.Clear();
            IPlugin[] plugins = Application.ActiveApplication.PluginManager.Plugins;
            foreach (IPlugin plugin in plugins)
            {
                DataRow dr = dt.NewRow();
                dr[0] = plugin.PluginInfo.Name;
                dr[1] = plugin.PluginInfo.Version;
                dr[2] = plugin.PluginInfo.FilePath;
                dt.Rows.Add(dr);
            }


            //string[] files = Directory.GetFiles(PluginPath);

            //foreach (string file in files)
            //{
            //    string ext = Path.GetExtension(file);
            //    if (ext != ".dll") continue;

            //    DataRow dr = dt.NewRow();
            //    string thisfile = Path.GetFileName(file);
            //    dr[0] = thisfile;
            //    dr[1] = FileVersionInfo.GetVersionInfo(file).FileVersion;/*AssemblyName.GetAssemblyName(file).Version; //获取版本号*/
            //    dr[2] = file;
            //    dt.Rows.Add(dr);

            //}
        }

        private void simpleButton3_Click(object sender, System.EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Multiselect = true;
            dlg.Filter = "支持格式(*.dll)|*.dll";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                string[] filePaths = dlg.FileNames;
                progressBar1.Properties.Maximum = filePaths.Count();
                progressBar1.Position = 0;
                progressBar1.Properties.Step = 1;

                string newFilePath;
                //创建文件夹
                if (dlg.FileNames.Length == 1)
                {
                    newFilePath = Path.Combine(PluginPath, Path.GetFileNameWithoutExtension(dlg.FileNames[0]));
                }
                else
                {
                    newFilePath = Path.Combine(PluginPath, "Unknown" + Directory.GetDirectories(Application.PluginDirPath).Length + 1);
                }
                Directory.CreateDirectory(newFilePath);
                foreach (string file in dlg.FileNames)
                {
                    File.Copy(file, newFilePath + "/" + Path.GetFileName(file));
                }
                string[] files = Directory.GetFiles(newFilePath);

                Application.ActiveApplication.PluginManager.AddPlugins(files);


                //foreach (string filePath in filePaths)
                //{
                //    try
                //    {
                //        //拼接地址
                //        File.Copy(filePath, newFilePath);
                //        progressBar1.PerformStep();
                //        DataRow dr = dt.NewRow();
                //        dr[0] = Path.GetFileName(filePath);
                //        dr[1] = FileVersionInfo.GetVersionInfo(filePath).FileVersion;/*AssemblyName.GetAssemblyName(newFilePath).Version;*/
                //        dr[2] = newFilePath;
                //        dt.Rows.Add(dr);
                //    }
                //    catch (Exception ext)
                //    {

                //        throw ext;
                //    }
                //}
                //InitPlugin();
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            InitPlugin();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            try
            {
                progressBar1.Properties.Maximum = 100;
                progressBar1.Position = 0;
                int a = gridView1.FocusedRowHandle;
                string filePath = gridView1.GetRowCellValue(a, "地址").ToString();

                if (!File.Exists(filePath))
                {
                    XtraMessageBox.Show("文件消失了！");
                    return;
                }
                try
                {
                    File.Delete(filePath);
                    progressBar1.Position = 50;

                    dt.Rows[a].Delete();
                    progressBar1.Position = 100;

                    DeleteList.Add(filePath);
                    XtraMessageBox.Show("移除成功！重启后生效！");
                }
                catch (Exception ext)
                {
                    MessageBox.Show(ext.ToString());
                }
            }
            catch (Exception ext)
            {
                if (ext.ToString().Contains("Null"))
                    XtraMessageBox.Show("没有条目，无法删除");
            }


        }
    }
}
