﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop.Theme
{
    public class Theme : TPlugin
    {
        public override TDockBar Initialize()
        {
            TDockBar dock = new TDockBar();
            dock.Caption = "主题设置";
            dock.Control = new ThemeForm();
            return dock;
        }
    }
}
