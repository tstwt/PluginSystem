﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TDesktop.Theme
{
    public partial class ThemeForm : UserControl
    {
        public ThemeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.ActiveApplication.MainForm.TStyle = ThemeStyle.Office2016Colorful;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.ActiveApplication.MainForm.TStyle = ThemeStyle.VisualStudio2013Dark;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.ActiveApplication.MainForm.TStyle = ThemeStyle.Office2010Blue;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.ActiveApplication.MainForm.TStyle = ThemeStyle.DevExpressStyle;
        }
    }
}
