﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace TDesktop.Comparison
{
    public partial class MainControl : UserControl
    {
        List<string> listStandardFiles = new List<string>();
        List<string> listComparisonFiles = new List<string>();

        public MainControl()
        {
            InitializeComponent();
        }

        private void checkBZ_Click(object sender, EventArgs e)
        {
            Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog open = new Microsoft.WindowsAPICodePack.Dialogs.CommonOpenFileDialog();
            open.IsFolderPicker = true;
            open.Multiselect = false;
            if (open.ShowDialog() != Microsoft.WindowsAPICodePack.Dialogs.CommonFileDialogResult.Ok)
            {
                return;
            }
            DirectoryInfo info = new DirectoryInfo(open.FileName);
            if (!info.Exists)
            {
                MessageBox.Show("文件夹不合法", "提示");
                return;
            }
            FileInfo[] files = info.GetFiles();
            foreach (FileInfo file in files)
            {
                listBZ.Items.Add(file.Name);
                listStandardFiles.Add(file.Name);
            }
        }

        private void checkDB_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog open = new CommonOpenFileDialog();
            open.IsFolderPicker = true;
            open.Multiselect = false;
            if (open.ShowDialog() != CommonFileDialogResult.Ok)
            {
                return;
            }
            DirectoryInfo info = new DirectoryInfo(open.FileName);
            if (!info.Exists)
            {
                MessageBox.Show("文件夹不合法", "提示");
                return;
            }
            FileInfo[] files = info.GetFiles();
            foreach (FileInfo file in files)
            {
                listDB.Items.Add(file.Name);
                listComparisonFiles.Add(file.Name);
            }
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            
            //开始
            foreach (string StandardFile in listStandardFiles)
            {
                if (listComparisonFiles.Contains(StandardFile))
                {
                    //如果存在，就删除两边
                    //listComparisonFiles.Remove(StandardFile);
                    //listStandardFiles.Remove(StandardFile);
                    listBZ.Items.Remove(StandardFile);
                    listDB.Items.Remove(StandardFile);
                }
                //Application.DoEvents();
            }
            groupBox1.Text = "缺少文件";
            groupBox2.Text = "多余文件";
            checkBZ.Enabled = false;
            checkDB.Enabled = false;
            //listQS.Items.AddRange(listBZ.Items);
            //listDY.Items.AddRange(listComparisonFiles.ToArray());
            MessageBox.Show("结束");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            groupBox1.Text = "标准文件";
            groupBox2.Text = "对比文件";
            checkBZ.Enabled = true;
            checkDB.Enabled = true;
            listComparisonFiles.Clear();
            listStandardFiles.Clear();
            listBZ.Items.Clear();
            listDB.Items.Clear();
            
        }
    }
}
