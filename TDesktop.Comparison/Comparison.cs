﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop.Comparison
{
    public class Comparison : TPlugin
    {
        public override TDockBar Initialize()
        {
            TDockBar dock = new TDockBar();
            dock.Caption = "文件对比";
            dock.Control = new MainControl();
            return dock;
        }
    }
}
