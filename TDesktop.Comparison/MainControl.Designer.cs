﻿namespace TDesktop.Comparison
{
    partial class MainControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBZ = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listDB = new System.Windows.Forms.ListBox();
            this.checkBZ = new System.Windows.Forms.Button();
            this.checkDB = new System.Windows.Forms.Button();
            this.btn_Start = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.listBZ);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(424, 112);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "标准文件";
            // 
            // listBZ
            // 
            this.listBZ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBZ.FormattingEnabled = true;
            this.listBZ.ItemHeight = 12;
            this.listBZ.Location = new System.Drawing.Point(3, 17);
            this.listBZ.Name = "listBZ";
            this.listBZ.Size = new System.Drawing.Size(418, 92);
            this.listBZ.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.listDB);
            this.groupBox2.Location = new System.Drawing.Point(3, 121);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(424, 123);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "对比文件";
            // 
            // listDB
            // 
            this.listDB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listDB.FormattingEnabled = true;
            this.listDB.ItemHeight = 12;
            this.listDB.Location = new System.Drawing.Point(3, 17);
            this.listDB.Name = "listDB";
            this.listDB.Size = new System.Drawing.Size(418, 103);
            this.listDB.TabIndex = 0;
            // 
            // checkBZ
            // 
            this.checkBZ.Location = new System.Drawing.Point(26, 259);
            this.checkBZ.Name = "checkBZ";
            this.checkBZ.Size = new System.Drawing.Size(99, 23);
            this.checkBZ.TabIndex = 1;
            this.checkBZ.Text = "选择标准文件夹";
            this.checkBZ.UseVisualStyleBackColor = true;
            this.checkBZ.Click += new System.EventHandler(this.checkBZ_Click);
            // 
            // checkDB
            // 
            this.checkDB.Location = new System.Drawing.Point(325, 259);
            this.checkDB.Name = "checkDB";
            this.checkDB.Size = new System.Drawing.Size(99, 23);
            this.checkDB.TabIndex = 1;
            this.checkDB.Text = "选择对比文件夹";
            this.checkDB.UseVisualStyleBackColor = true;
            this.checkDB.Click += new System.EventHandler(this.checkDB_Click);
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(26, 307);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(99, 23);
            this.btn_Start.TabIndex = 1;
            this.btn_Start.Text = "开始";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(325, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "还原";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.checkDB);
            this.Controls.Add(this.checkBZ);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(560, 632);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBZ;
        private System.Windows.Forms.ListBox listDB;
        private System.Windows.Forms.Button checkBZ;
        private System.Windows.Forms.Button checkDB;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Button button1;
    }
}
