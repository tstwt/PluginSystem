﻿using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using System.Drawing.Drawing2D;
using System;
using System.Globalization;

namespace TDesktop
{
    class _SplashForm : ISplashForm
    {
        private SplashForm m_form;
        private Bitmap m_backgroundImage;

        private Thread m_thread;

        private delegate void _CloseForm();

        internal _SplashForm()
        {
            m_form = new SplashForm();
            m_form.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            m_form.FormBorderStyle = FormBorderStyle.None;
            m_form.TopMost = true;
            m_form.ShowInTaskbar = false;
            m_form.Load += new EventHandler(m_form_Load);
        }

        private void StartShow()
        {
            System.Windows.Forms.Application.Run(m_form);
        }

        private void m_form_Load(object sender, EventArgs e)
        {
            m_form.Image = m_backgroundImage;
            if (m_form.Image == null)
            {
                m_form.Size = new Size(0, 0);
            }
            else
            {
                //这里需要重新计算一下窗口的location，因为窗口大小改变了
                Size size = m_form.ClientSize;
                Point location = m_form.Location;
                m_form.ClientSize = m_backgroundImage.Size;
                m_form.Location = new Point(location.X - (m_form.ClientSize - size).Width / 2,
                                            location.Y - (m_form.ClientSize - size).Height / 2);
            }
        }

        public void Show()
        {
            m_form.TopMost = true;

            m_thread = CommonToolkit.ThreadWrap.CreateThread(StartShow);
            m_thread.Start();
        }

        private void CloseForm()
        {
            m_form.Close();
        }

        private void HideForm()
        {
            m_form.Hide();
        }

        public void Hide()
        {
            _CloseForm closeForm = new _CloseForm(this.HideForm);
            IAsyncResult result = m_form.BeginInvoke(closeForm, null);
        }

        public void Close()
        {
            _CloseForm closeForm = new _CloseForm(this.CloseForm);
            IAsyncResult result = m_form.BeginInvoke(closeForm, null);
        }

        public Bitmap BackgroundImage
        {
            get
            {
                return m_backgroundImage;
            }
            set
            {
                m_backgroundImage = value;
            }
        }

        public Control.ControlCollection Controls
        {
            get
            {
                return m_form.Controls;
            }
        }
    }
}