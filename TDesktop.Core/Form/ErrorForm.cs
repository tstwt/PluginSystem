using System;
using System.Windows.Forms;

namespace TDesktop
{
    public partial class ErrorForm : Form
    {
        public static void ShowErrorMessage(string text, string message, string detail)
        {
            ErrorForm frm = new ErrorForm();
            frm.Text = text;
            frm.ErrorMessage = message;
            frm.ErrorDetail = detail;
            frm.ShowDialog();
        }

        public ErrorForm()
        {
            InitializeComponent();
        }

        private void ErrorForm_Load(object sender, EventArgs e)
        {
            errorDetailTextBox.Visible = false;
        }

        public string ErrorMessage
        {
            get { return errorMessageLabel.Text; }
            set { errorMessageLabel.Text = value; }
        }

        public string ErrorDetail
        {
            get { return errorDetailTextBox.Text; }
            set { errorDetailTextBox.Text = value; }
        }

        public string Caption
        {
            get { return this.Text; }
            set { this.Text = value; }
        }

        private void expandButton_Click(object sender, EventArgs e)
        {
            errorDetailTextBox.Visible = !errorDetailTextBox.Visible;
        }

        private void errorDetailTextBox_VisibleChanged(object sender, EventArgs e)
        {
            expandButton.Text = errorDetailTextBox.Visible ? "��ϸ��Ϣ��" : "��ϸ��Ϣ��";
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(this.Close));
            }
            else
            {
                this.Close();
            }
        }
    }
}