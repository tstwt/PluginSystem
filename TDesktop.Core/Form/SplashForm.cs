﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Globalization;

namespace TDesktop
{
    public partial class SplashForm : Form
    {
        private Image m_image = null;
        public Image Image
        {
            get
            {
                return m_image;
            }
            set
            {
                m_image = value;
            }
        }
        private EventHandler m_eventAnimator = null;
        public SplashForm()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            m_eventAnimator = new EventHandler(OnImageAnimate);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //this.TopMost = true;
            if (m_image != null)
            {
                UpdateImage();
                e.Graphics.DrawImage(m_image, new Rectangle(0, 0, m_image.Width, m_image.Height));
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //this.TopMost = true;
            if (m_image != null)
            {
                BeginAnimate();
            }            
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            if (m_image != null)
            {
                StopAnimate();
                m_image = null;
            }
        }

        private void OnImageAnimate(Object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void UpdateImage()
        {
            if (m_image != null)
            {
                if (ImageAnimator.CanAnimate(m_image))
                {
                    ImageAnimator.UpdateFrames(m_image);
                }
            }
        }

        private void BeginAnimate()
        {
            if (m_image != null)
            {
                if (ImageAnimator.CanAnimate(m_image))
                {
                    ImageAnimator.Animate(m_image, m_eventAnimator);
                }
            }
        }

        private void StopAnimate()
        {
            if (m_image != null)
            {
                if (ImageAnimator.CanAnimate(m_image))
                {
                    ImageAnimator.StopAnimate(m_image, m_eventAnimator);
                }
            }
        }
    }
}
