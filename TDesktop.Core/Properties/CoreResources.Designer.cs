﻿//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

namespace TDesktop.Properties {
    using System;
    
    
    /// <summary>
    ///   一个强类型的资源类，用于查找本地化的字符串等。
    /// </summary>
    // 此类是由 StronglyTypedResourceBuilder
    // 类通过类似于 ResGen 或 Visual Studio 的工具自动生成的。
    // 若要添加或移除成员，请编辑 .ResX 文件，然后重新运行 ResGen
    // (以 /str 作为命令选项)，或重新生成 VS 项目。
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CoreResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CoreResources() {
        }
        
        /// <summary>
        ///   返回此类使用的缓存的 ResourceManager 实例。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TDesktop.Properties.CoreResources", typeof(CoreResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   使用此强类型资源类，为所有资源查找
        ///   重写当前线程的 CurrentUICulture 属性。
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   查找 System.Drawing.Bitmap 类型的本地化资源。
        /// </summary>
        internal static System.Drawing.Bitmap _150476575066004 {
            get {
                object obj = ResourceManager.GetObject("_150476575066004", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   查找 System.Drawing.Bitmap 类型的本地化资源。
        /// </summary>
        internal static System.Drawing.Bitmap Logo {
            get {
                object obj = ResourceManager.GetObject("Logo", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   查找类似 . 的本地化字符串。
        /// </summary>
        internal static string String_Dot {
            get {
                return ResourceManager.GetString("String_Dot", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 动态库“{0}”中的“{1}”类实例化失败 的本地化字符串。
        /// </summary>
        internal static string String_Exception_CreatePlugin {
            get {
                return ResourceManager.GetString("String_Exception_CreatePlugin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 动态库路径“{0}”不存在 的本地化字符串。
        /// </summary>
        internal static string String_Exception_PathNotExist {
            get {
                return ResourceManager.GetString("String_Exception_PathNotExist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 - 的本地化字符串。
        /// </summary>
        internal static string String_Minus {
            get {
                return ResourceManager.GetString("String_Minus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 + 的本地化字符串。
        /// </summary>
        internal static string String_ShortCut_Between {
            get {
                return ResourceManager.GetString("String_ShortCut_Between", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 Ctrl 的本地化字符串。
        /// </summary>
        internal static string String_ShortCut_Ctrl {
            get {
                return ResourceManager.GetString("String_ShortCut_Ctrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 ] 的本地化字符串。
        /// </summary>
        internal static string String_ShortCutEnd {
            get {
                return ResourceManager.GetString("String_ShortCutEnd", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 + 的本地化字符串。
        /// </summary>
        internal static string String_ShortCutKeySplit {
            get {
                return ResourceManager.GetString("String_ShortCutKeySplit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 [ 的本地化字符串。
        /// </summary>
        internal static string String_ShortCutStart {
            get {
                return ResourceManager.GetString("String_ShortCutStart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 ) 的本地化字符串。
        /// </summary>
        internal static string String_ShortCutString_End {
            get {
                return ResourceManager.GetString("String_ShortCutString_End", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 ( 的本地化字符串。
        /// </summary>
        internal static string String_ShortCutString_Start {
            get {
                return ResourceManager.GetString("String_ShortCutString_Start", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 天 的本地化字符串。
        /// </summary>
        internal static string String_Time_Days {
            get {
                return ResourceManager.GetString("String_Time_Days", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 小时 的本地化字符串。
        /// </summary>
        internal static string String_Time_Hours {
            get {
                return ResourceManager.GetString("String_Time_Hours", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 分 的本地化字符串。
        /// </summary>
        internal static string String_Time_Minutes {
            get {
                return ResourceManager.GetString("String_Time_Minutes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 秒 的本地化字符串。
        /// </summary>
        internal static string String_Time_Seconds {
            get {
                return ResourceManager.GetString("String_Time_Seconds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   查找类似 &lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt; 的本地化字符串。
        /// </summary>
        internal static string String_XMLVersion {
            get {
                return ResourceManager.GetString("String_XMLVersion", resourceCulture);
            }
        }
    }
}
