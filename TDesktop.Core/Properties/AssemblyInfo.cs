﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("用户沟通记录")]
[assembly: AssemblyDescription("框架核心代码")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("唐唯添")]
[assembly: AssemblyProduct("用户沟通记录")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// 将 ComVisible 设置为 false 会使此程序集中的类型
//对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("66bf89ee-5ee5-48c8-8675-6a2425bb9679")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 可以指定所有值，也可以使用以下所示的 "*" 预置版本号和修订号
// 方法是按如下所示使用“*”: :
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
// 这两一定不要变，否则引用本Assembly的程序必须重新编译 [2017-8-28 Tangweitian]

// 如果要修改插件产品版本号，则修改这个 [2017-8-28 Tangweitian]
[assembly: AssemblyInformationalVersion("1.0.2.17626")]

[assembly: InternalsVisibleTo("TDesktop,PublicKey=0024000004800000940000000602000000240000525341310004000001000100a92876c17e0ec6ba874dbf175ccbd1cfae8ea372cf8af5686efc8740c8580bae0f9453ecb67370a25b4066bbdfd96cdace01793fa98b1c365445758788a49bc5b355e06ff7639223d537b2069f8ad68394320185b5282aed36531ef8e8e5ee181400eb2772c6804fa078f00ea5fd065bd0522fc4c07685339c198a7f56c9f5a6")]

[assembly: InternalsVisibleTo("TDesktop.Startup,PublicKey=0024000004800000940000000602000000240000525341310004000001000100a92876c17e0ec6ba874dbf175ccbd1cfae8ea372cf8af5686efc8740c8580bae0f9453ecb67370a25b4066bbdfd96cdace01793fa98b1c365445758788a49bc5b355e06ff7639223d537b2069f8ad68394320185b5282aed36531ef8e8e5ee181400eb2772c6804fa078f00ea5fd065bd0522fc4c07685339c198a7f56c9f5a6")]
