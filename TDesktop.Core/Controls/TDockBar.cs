﻿/**************************************************************** 
 * 作    者：TangWeitian 
 * CLR 版本：4.0.30319.42000 
 * 创建时间：2017/6/24 14:58:37 
 * 当前版本：1.0.0.0
 * 描述说明：DockPanel
 * 
 * 修改历史： 
 * 
***************************************************************** 
 * Copyright @ TangWeitian 2017 All rights reserved 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking;

namespace TDesktop.Controls
{
    public class TDockBar
    {
        #region 字段

        protected ControlContainer m_container;
        protected DockPanel m_dockpanel;

        #endregion

        #region 属性

        /// <summary>
        /// 标题名称
        /// </summary>
        public String Caption
        {
            get { return m_dockpanel.Text; }
            set { m_dockpanel.Text = value; }
        }

        /// <summary>
        /// 依附模式
        /// </summary>
        public DockingStyle DockStyle
        {
            get { return m_dockpanel.Dock; }
            set { m_dockpanel.Dock = value; }
        }

        /// <summary>
        /// 大小
        /// </summary>
        public new Size Size
        {
            get { return m_dockpanel.Size; }
            set
            {
                m_dockpanel.Size = value;
                m_dockpanel.FloatSize = value;
            }
        }

        /// <summary>
        /// 其中的Control
        /// </summary>
        public UserControl Control
        {
            get
            {
                try
                {
                    Control control = null;
                    if (m_container.Controls.Count == 1)
                    {
                        control = m_container.Controls[0];
                    }
                }
                catch
                {
                }
                return Control;
            }
            set
            {
                m_container.Controls.Clear();
                m_container.Controls.Add(value);
                value.Dock = System.Windows.Forms.DockStyle.Fill;
            }
        }

        /// <summary>
        /// 是否当做标签
        /// </summary>
        public Boolean DockedAsTab
        {
            get { return m_dockpanel.DockedAsTabbedDocument; }
            set { m_dockpanel.DockedAsTabbedDocument = value; }
        }


        internal DockPanel DockPanel
        {
            get { return m_dockpanel;}
        }

        #endregion

        #region 构建函数

        public TDockBar()
        {
            Initialize();
        }

        protected Boolean Initialize()
        {
            m_dockpanel = new DockPanel();
            if (m_dockpanel.FloatForm != null)
            {
                m_dockpanel.FloatForm.Opacity = 0.5;
            }

            m_container = new ControlContainer();
            m_dockpanel.Controls.Add(m_container);
            m_dockpanel.DockedAsTabbedDocument = true;
            m_dockpanel.Dock = DockingStyle.Right;
            m_dockpanel.Options.ShowCloseButton = false;
            this.m_dockpanel.ClosedPanel += MDockpanelOnClosedPanel;

            return true;
        }



        #endregion

        #region 内部方法

        private void MDockpanelOnClosedPanel(object sender, DockPanelEventArgs e)
        {
            if (Closed != null)
            {
                Closed(this, new TDockBarClosedEventArgs());
            }
        }

        #endregion

        #region 外部方法



        #endregion

        #region Event_Members

        public event TDockBarClosedEventHandler Closed;

        #endregion

        /// <summary>
        /// Dockbar关闭中触发的委托类
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void TDockBarClosedEventHandler(object sender, TDockBarClosedEventArgs e);

        /// <summary>
        /// Dockbar关闭中触发的数据类
        /// </summary>
        public class TDockBarClosedEventArgs : EventArgs
        {
            //public Boolean m_cancel;

            ///// <summary>
            ///// 取消关闭
            ///// </summary>
            //public bool Cancel
            //{
            //    get { return m_cancel; }
            //    set { m_cancel = value; }
            //}

            public TDockBarClosedEventArgs()
            {
            }
        }
    }
}
