﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTab;

namespace TDesktop
{
    public interface IMainForm
    {

        /// <summary>
        /// 改变软件的主题
        /// </summary>
        ThemeStyle TStyle { get; set; }

        /// <summary>
        /// 加载主窗口界面元素后将触发该事件。 
        /// </summary>
        event EventHandler Loaded;

        /// <summary>
        /// 加载主窗口界面元素时将触发该事件。
        /// </summary>
        event EventHandler Loading;

        /// <summary>
        /// 主题风格变化时将触发该事件。
        /// </summary>
        event ThemeStyleChangedEventHandler ThemeStyleChanged;

        /// <summary>
        /// 界面大小改变时将触发改事件
        /// </summary>
        event SizeChangingEventHandler SizeChanging;

        /// <summary>
        /// 窗口关闭时触发的事件
        /// </summary>
        event EventHandler<CancelEventArgs> MainClosing;

        /// <summary>
        /// 窗口关闭后触发的事件
        /// </summary>
        event EventHandler MainClosed;
    }
}
