﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace TDesktop
{
    /// <summary>
    /// 定义启动界面所具有的功能的接口。
    /// </summary>
    public interface ISplashForm
    {
        /// <summary>
        /// 显示启动界面。
        /// </summary>
        void Show();

        /// <summary>
        /// 隐藏启动界面。 
        /// </summary>
        void Hide();

        /// <summary>
        /// 关闭启动界面。
        /// </summary>
        void Close();
        
        /// <summary>
        /// 获取或设置启动界面的背景图片。
        /// </summary>
        Bitmap BackgroundImage
        {
            get;
            set;
        }
        
        /// <summary>
        /// 获取启动界面所使用的所有控件的集合。 
        /// </summary>
        Control.ControlCollection Controls
        {
            get;
        }
    }
}
