﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop
{
    public interface IPlugin
    {
        /// <summary>
        /// 设置Tab中的Control
        /// </summary>
        /// <returns></returns>
        TDockBar Initialize();


        /// <summary>
        /// 获取插件信息
        /// </summary>
        TPluginInfo PluginInfo { get; }
    }
}
