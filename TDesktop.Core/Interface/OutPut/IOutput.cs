﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace TDesktop
{
    /// <summary>
    /// 表示 IOutput.Outputting 事件使用的委托类型。
    /// </summary>
    /// <param name="sender">事件源</param>
    /// <param name="e">包含事件数据的 OutputEventArgs</param>
    public delegate void OutputtingEventHandler(object sender, OutputtingEventArgs e);

    /// <summary>
    /// 为 IOutput.Outputting 事件提供数据。
    /// </summary>
    public class OutputtingEventArgs : CancelEventArgs
    {
        /// <summary>
        /// 构造一个新的 OutputEventArgs 对象。
        /// </summary>
        public OutputtingEventArgs()
        {
            m_message = String.Empty;
        }

        String m_message;
        /// <summary>
        /// 获取或设置当前输出信息的内容。
        /// </summary>
        public String Message
        {
            get
            {
                return m_message;
            }
            set
            {
                m_message = value;
            }
        }

        InfoType m_type;
        /// <summary>
        /// 获取或设置当前输出信息的类型。
        /// </summary>
        public InfoType InfoType
        {
            get
            {
                return m_type;
            }
            set
            {
                m_type = value;
            }
        }
    }

    /// <summary>
    /// 定义信息输出接口。
    /// </summary>
    public interface IOutput
    {
        /// <summary>
        /// 获取指定索引的输出信息。
        /// </summary>
        /// <param name="line">输出信息索引</param>
        /// <returns>输出信息</returns>
        String this[Int32 line]
        {
            get;
        }

        /// <summary>
        /// 获取输出信息的条数。 
        /// </summary>
        Int32 LineCount
        {
            get;
        }

        /// <summary>
        /// 获取或设置最大的输出信息条数。
        /// </summary>
        Int32 MaxLineCount
        {
            get;
            set;
        }

        /// <summary>
        /// 获取或设置输出信息的输出框中的内容在必要时是否自动换行显示。 
        /// </summary>
        Boolean IsWordWrapped
        {
            get;
            set;
        }

        /// <summary>
        /// 获取或设置是否在输出信息前添加信息输出的时间。
        /// </summary>
        Boolean IsTimePrefixAdded
        {
            get;
            set;
        }

        /// <summary>
        /// 获取或设置信息输出时间的格式。
        /// </summary>
        String TimePrefixFormat
        {
            get;
            set;
        }
        
        /// <summary>
        /// 将指定的输出信息进行输出。 
        /// </summary>
        /// <param name="message">输出信息</param>
        void Output(String message);

        /// <summary>
        /// 将指定的输出信息按指定输出类型进行输出。 
        /// </summary>
        /// <param name="message">输出信息</param>
        /// <param name="Type">信息输出类型</param>
        void Output(String message, InfoType Type);

        /// <summary>
        /// 清空输出信息。
        /// </summary>
        void ClearOutput();

        /// <summary>
        /// 当进行输出信息的输出时将触发该事件。 
        /// </summary>
        event OutputtingEventHandler Outputting;
    }
}
