﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop
{
    public interface IPluginManager
    {
        /// <summary>
        /// 插件集合
        /// </summary>
        IPlugin[] Plugins { get; }

        /// <summary>
        /// 添加插件的时候
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        IPlugin AddPlugin(string filePath);

        /// <summary>
        /// 添加多个插件
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        IPlugin[] AddPlugins(string[] filePath);

        /// <summary>
        /// 移除插件
        /// </summary>
        /// <param name="plugin"></param>
        /// <returns></returns>
        Boolean Remove(IPlugin plugin);

        /// <summary>
        /// 插件加载前将触发该事件。
        /// </summary>
        event PluginLoadingEventHandler PluginLoading;

        /// <summary>
        /// 插件加载完毕时将触发该事件。
        /// </summary>
        event PluginLoadedEventHandler PluginLoaded;

        /// <summary>
        /// 插件卸载前将触发该事件。
        /// </summary>
        [Obsolete("没有实现", false)]
        event PluginUnLoadingEventHandler PluginUnloading;

        /// <summary>
        /// 插件卸载完毕时将触发该事件。
        /// </summary>
        [Obsolete("没有实现", false)]
        event PluginUnLoadedEventHandler PluginUnloaded;

    }
}
