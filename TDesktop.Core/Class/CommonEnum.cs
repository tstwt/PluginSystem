﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDesktop
{
    /// <summary>
    /// 输出信息的类型。
    /// </summary>
    public enum InfoType
    {
        /// <summary>
        /// 信息类型。
        /// </summary>
        Information = 1,

        /// <summary>
        /// 异常类型。
        /// </summary>
        Exception = 2
    }


    //以下是关于监视各种键盘输入操作的功能        
    internal enum KeyMessage
    {
        WM_KEYDOWN = 0x0100,
        WM_KEYUP = 0x0101,
        WM_SYSKEYDOWN = 0x0104,
        WM_SYSKEYUP = 0x0105,
    }

    /// <summary>
    /// 主题
    /// </summary>
    public enum ThemeStyle
    {
        Office2016Colorful,
        VisualStudio2013Dark,
        Office2010Blue,
        DevExpressStyle
    }
}
