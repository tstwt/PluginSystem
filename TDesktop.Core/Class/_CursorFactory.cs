﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace TDesktop
{
    class _CursorFactory
    {
        [DllImport("USER32.DLL")]
        public static extern Boolean DestroyCursor(IntPtr intPtr);
        static private Dictionary<Cursor, IntPtr> m_dicCursors = new Dictionary<Cursor, IntPtr>();

        public static void DestroyCursors()
        {
            try
            {
                foreach (IntPtr ptr in m_dicCursors.Values)
                {
                    DestroyCursor(ptr);
                }
                m_dicCursors.Clear();
            }
            catch
            {
            }
        }
    }
}
