﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDesktop
{
    class ConcreteOutPut : IOutput
    {
        public string this[int line] => throw new NotImplementedException();

        public int LineCount => throw new NotImplementedException();

        public int MaxLineCount { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool IsWordWrapped { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool IsTimePrefixAdded { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string TimePrefixFormat { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public event OutputtingEventHandler Outputting;

        public void ClearOutput()
        {
            throw new NotImplementedException();
        }

        public void Output(string message)
        {
            throw new NotImplementedException();
        }

        public void Output(string message, InfoType pType)
        {
            DateTime time = DateTime.Now;

            String fileName = String.Format("{0}CrashDump-{1}-{2}-{3}-{4}-{5}-{6}-{7}.log",
                                            AppDomain.CurrentDomain.BaseDirectory,
                                            time.Year,
                                            time.Month,
                                            time.Day,
                                            time.Hour,
                                            time.Minute,
                                            time.Second,
                                            time.Millisecond);

            StreamWriter writer = File.CreateText(fileName);
            writer.WriteLine("Message:");
            writer.WriteLine(message);


            writer.Close();
        }
          
    }
}
