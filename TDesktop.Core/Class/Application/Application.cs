﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTab;
using TDesktop.Properties;

namespace TDesktop
{
    /// <summary>
    /// 应用程序类，实现启动主窗口、插件管理和代码段编译执行等功能。
    /// </summary>
    public class Application : IDisposable
    {

        #region 初始化事件

        /// <summary>
        /// 创建一个应用程序类的新实例
        /// </summary>
        public Application()
        {
            m_splashForm = new _SplashForm();
            m_splashForm.BackgroundImage = CoreResources.Logo;
            m_args = new List<String>();
        }

        #endregion

        #region 属性

        private TPluginManager m_PluginManager;

        #endregion

        #region 构造事件

        /// <summary>
        /// 初始化应用程序。
        /// </summary>
        public void Initialize()
        {
            try
            {
                //#if !DEBUG
                //挂接键盘钩子
                CommonToolkit.KeyboardWrap.HookWindowsHookEx();
                //#endif
                DirectoryInfo dirinfo = new DirectoryInfo(Tag.PluginDirPath);
                if (!dirinfo.Exists)
                {
                    dirinfo.Create();
                }
                
                //加载插件管理器
                m_PluginManager = new TPluginManager();

                IPlugin[] TPlugins = m_PluginManager.Load();

                //打开主窗口
                this.MainForm = new MainFrm(TPlugins);


                Initialized?.Invoke(this, new EventArgs());




                //
                //3、结束启动画面，如果主窗口启动比较慢，需要再晚一点才干掉  
            }
            catch (LicenseException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 启动应用程序。
        /// </summary>
        public void Run()
        {
            if (this.MainForm == null)
            {
                throw new NullReferenceException("Application.ActiveApplication.MainForm is NULL.");
            }
            else
            {
                System.Windows.Forms.Application.Run(this.MainForm as Form);
            }

            Dispose();
        }

        #endregion

        #region 属性

        /// <summary>
        /// 插件管理器
        /// </summary>
        public IPluginManager PluginManager
        {
            get => m_PluginManager;
        }

        private static Application m_activeApplication = null;


        /// <summary>
        /// 获取或设置当前所有 Desktop.Application 中当前活动的 Application。
        /// </summary>
        public static Application ActiveApplication
        {
            get
            {
                return m_activeApplication;
            }

            set
            {
                m_activeApplication = value;
            }
        }

        private IOutput m_output = new ConcreteOutPut();
        /// <summary>
        /// 获取或设置应用程序的输出信息对象。 
        /// </summary>
        internal IOutput Output
        {
            get
            {
                return m_output;
            }

            set
            {
                m_output = value;
            }
        }

        // 程序启动时的参数
        private List<String> m_args;
        internal String[] Args
        {
            get
            {
                return m_args.ToArray();
            }
            set
            {
                m_args.Clear();
                if (value != null)
                {
                    m_args.AddRange(value);
                }
            }
        }

        private IMainForm m_mainForm;
        /// <summary>
        /// 获取或设置应用程序的主窗口。 
        /// </summary>
        public IMainForm MainForm
        {
            get
            {
                return m_mainForm;
            }

            set
            {
                m_mainForm = value;
            }
        }

        /// <summary>
        /// 应用程序启动路径
        /// </summary>
        public static string StartupPath
        {
            get { return System.Windows.Forms.Application.StartupPath; }
        }

        /// <summary>
        /// 插件文件夹地址
        /// </summary>
        public static string PluginDirPath
        {
            get { return Tag.PluginDirPath; }
        }

        private ISplashForm m_splashForm;
        /// <summary>
        /// 获取或设置应用程序的启动界面
        /// </summary>
        public ISplashForm SplashForm
        {
            get => m_splashForm;
            set => m_splashForm = value;
        }

        #endregion

        #region 公共事件

        // 反注册键盘钩子
        /// <summary>
        /// 退出应用程序。
        /// </summary>
        public void Exit()
        {
            //#if !DEBUG
            //取消对键盘钩子的挂接
            CommonToolkit.KeyboardWrap.UnhookWindowsHookEx();
            //#endif

        }

        public void Dispose()
        {
            _CursorFactory.DestroyCursors();
        }

        #endregion

        #region Event_Number

        /// <summary>
        /// 当应用程序初始化完成后将触发该事件。
        /// </summary>
        public event EventHandler Initialized;

        #endregion

    }
}
