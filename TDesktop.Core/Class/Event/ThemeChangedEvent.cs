﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDesktop
{
    /// <summary>
    /// 改变主题后的委托类
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ThemeStyleChangedEventHandler(Object sender, ThemeStyleChangedEventArgs e);

    /// <summary>
    /// 改变主题后的数据类
    /// </summary>
    public class ThemeStyleChangedEventArgs : EventArgs
    {
        private ThemeStyle m_ThemeStyle;

        public ThemeStyle ThemeStyle
        {
            get { return m_ThemeStyle; }
            set { m_ThemeStyle = value; }
        }

        /// <summary>
        /// 构建一个改变主题后的数据类
        /// </summary>
        /// <param name="ts"></param>
        public ThemeStyleChangedEventArgs(ThemeStyle ts)
        {
            m_ThemeStyle = ts;
        }
    }
}
