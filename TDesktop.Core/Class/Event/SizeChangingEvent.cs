﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TDesktop
{
    /// <summary>
    /// 改变主题后的委托类
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SizeChangingEventHandler(Object sender, SizeChangingEventArgs e);

    /// <summary>
    /// 改变主题后的数据类
    /// </summary>
    public class SizeChangingEventArgs : EventArgs
    {
        private Size m_Size;

        public Size Size
        {
            get { return m_Size; }
            set { m_Size = value; }
        }

        /// <summary>
        /// 构建一个改变主题后的数据类
        /// </summary>
        /// <param name="ts"></param>
        public SizeChangingEventArgs(Size size)
        {
            m_Size = size;
        }
    }
}
