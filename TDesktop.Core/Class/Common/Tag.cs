﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TDesktop
{
    internal class Tag
    {

        private static String m_PluginDirPath = Path.Combine(Application.StartupPath, "Plugins\\");

        /// <summary>
        /// 插件文件夹地址
        /// </summary>
        public static string PluginDirPath
        {
            get { return m_PluginDirPath; }
            set { m_PluginDirPath = value; }
        }
    }
}
