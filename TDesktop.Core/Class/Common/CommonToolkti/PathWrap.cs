﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class PathWrap
        {
            /// <summary>
            /// 获取指定相对路径的绝对路径
            /// </summary>
            internal static String GetFullPathName(String pathName)
            {
                return GetFullPathName(AppDomain.CurrentDomain.BaseDirectory, pathName);
            }

            /// <summary>
            /// 获取指定相对路径相对于指定根目录的绝对路径
            /// </summary>
            internal static String GetFullPathName(String rootPath, String pathName)
            {
                String newPathName = pathName;
                try
                {
                    if (pathName != String.Empty)
                    {
                        //有些时候，用户会修改我们的当前路径
                        //这样我们的一些基于相对路径的操作就不能成功            
                        String oldCurrentDirectory = System.Environment.CurrentDirectory;
                        System.Environment.CurrentDirectory = rootPath;
                        newPathName = Path.GetFullPath(pathName);

                        //为了友好起见，恢复一下以前的当前路径
                        System.Environment.CurrentDirectory = oldCurrentDirectory;
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }
                return newPathName;
            }

            /// <summary>
            /// 获取指定绝对路径相对AppDomain.CurrentDomain.BaseDirectory的相对路径
            /// </summary>
            internal static String GetRelativePath(String pathName)
            {
                return GetRelativePath(AppDomain.CurrentDomain.BaseDirectory, pathName);
            }

            /// <summary>
            /// 获取指定绝对路径相对指定根目录的相对路径
            /// </summary>
            internal static string GetRelativePath(String rootPath, String pathName)
            {
                String result = pathName;

                try
                {
                    Char[] chars = new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar };
                    String[] baseDirectory = rootPath.Split(chars);
                    String[] pathStrings = pathName.Split(chars);
                    Int32 count = Math.Min(baseDirectory.Length, pathStrings.Length);
                    Int32 index = -1;
                    for (Int32 i = 0; i < count; i++)
                    {
                        if (!baseDirectory[i].Equals(pathStrings[i]))
                        {
                            break;
                        }
                        index = i;
                    }
                    if (index > -1)
                    {
                        String before = String.Empty;
                        for (Int32 i = index + 1; i < baseDirectory.Length; i++)
                        {
                            if (!baseDirectory[i].Equals(""))
                            {
                                String temp = ".." + Path.DirectorySeparatorChar;
                                before += temp;
                            }
                        }
                        String after = "";
                        if (index + 1 < pathStrings.Length)
                        {
                            after = pathStrings[index + 1];
                            for (Int32 i = index + 2; i < pathStrings.Length; i++)
                            {
                                after = after + Path.DirectorySeparatorChar + pathStrings[i];
                            }
                        }
                        result = before + after;
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }
                return result;
            }

            /// <summary>
            /// 合并多个路径字符串
            /// </summary>
            internal static String CombinePath(params String[] paths)
            {
                String result = GetFullPathName(paths[0]);
                try
                {
                    for (Int32 i = 1; i < paths.Length; i++)
                    {
                        result = Path.Combine(result, paths[i]);
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }
                return result;
            }

            /// <summary>
            /// 获取某个目录下可用的文件名
            /// </summary>
            /// <param name="root">目录</param>
            /// <param name="fileName">文件名称前缀</param>
            /// <param name="postfix">文件的扩展名</param>
            /// <param name="index">文件名的搜寻开始序号</param>
            /// <returns></returns>
            internal static String GetAvailableFileName(String root, String fileName, String postfix, Int32 index)
            {
                String name = String.Empty;
                try
                {
                    String fullName = String.Empty;
                    if (index == 0)
                    {
                        fullName = fileName + postfix;
                    }
                    else
                    {
                        fullName = fileName + index + postfix;
                    }
                    String path = root + "//" + fullName;
                    if (File.Exists(path))
                    {
                        index++;
                        name = GetAvailableFileName(root, fileName, postfix, index);
                    }
                    else
                    {
                        name = Path.GetFileNameWithoutExtension(path);
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }
                return name;
            }

            /// <summary>
            /// 比较两个路径是否相等，比较相对路径之前得
            /// 先调用GetFullPath()转为绝对路径
            /// </summary>
            /// <param name="path1"></param>
            /// <param name="path2"></param>
            /// <returns></returns>
            internal static Boolean IsPathEquals(String path1, String path2)
            {
                Boolean result = false;
                try
                {
                    path1 = path1.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                    if (path1.EndsWith(Path.DirectorySeparatorChar.ToString()))
                    {
                        path1 = path1.Remove(path1.Length - 1);

                    }
                    path2 = path2.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
                    if (path2.EndsWith(Path.DirectorySeparatorChar.ToString()))
                    {
                        path2 = path2.Remove(path2.Length - 1);
                    }
                    result = (path1.Equals(path2, StringComparison.OrdinalIgnoreCase));
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }
                return result;
            }
        }
    }
}
