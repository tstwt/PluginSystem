﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class AssemblyWrap
        {
            const String g_strDesktopPublicKeyToken = "3c17054cff8d2f30";

            /// <summary>
            /// 根据程序集的显示名称判断是否LocaSpace程序集
            /// </summary>
            internal static Boolean IsSuperMapAssembly(String strFullName)
            {
                Boolean bIsSuperMapAssembly = false;
                try
                {
                    String[] s = strFullName.Split(',');
                    for (Int32 i = 0; i < s.Length; i++)
                    {
                        String str = s[i].TrimStart();
                        if (str.StartsWith("PublicKeyToken"))
                        {
                            String strPublicKeyToken = str.Replace("PublicKeyToken=", "");
                            if (strPublicKeyToken.Equals(g_strDesktopPublicKeyToken))
                            {
                                bIsSuperMapAssembly = true;
                            }
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
                return bIsSuperMapAssembly;
            }

        }
    }
}
