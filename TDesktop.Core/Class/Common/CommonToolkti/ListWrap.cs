﻿/**************************************************************** 
 * 作    者：TangWeitian 
 * CLR 版本：4.0.30319.42000 
 * 创建时间：2017/8/23 15:35:45 
 * 当前版本：1.0.0.0
 *
 * 描述说明：
 * 
 * 修改历史： 
 * 
***************************************************************** 
 * Copyright @ TangWeitian 2017 All rights reserved 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class ListWrap
        {
            /// <summary>
            /// 将List内容写入Txt
            /// </summary>
            /// <param name="list"></param>
            /// <param name="txtPath"></param>
            internal static void ListToFile(List<string> list, string txtPath)
            {
                FileStream fs = new FileStream(txtPath, FileMode.Create, FileAccess.Write);

                StreamWriter sw = new StreamWriter(fs);
                sw.Flush();
                //写入内容
                sw.BaseStream.Seek(0, SeekOrigin.Begin);
                List<string> temp = new List<string>();

                for (int i = 0; i < list.Count; i++)
                {
                    //if (File.Exists(list[i]))
                    {
                        string st = CommonToolkit.PathWrap.GetRelativePath(list[i]);
                        if (!temp.Contains(st))
                        {
                            temp.Add(st);
                            sw.WriteLine(st);
                        }
                    }
                }

                sw.Flush();
                sw.Close();
                fs.Close();
            }

            internal static List<string> FileToList(string txtPath)
            {
                try
                {
                    FileStream fs = new FileStream(txtPath, FileMode.Open, FileAccess.Read);
                    List<string> list = new List<string>();
                    StreamReader sr = new StreamReader(fs);
                    //读取文件内容
                    sr.BaseStream.Seek(0, SeekOrigin.Begin);
                    //从数据流中读取每一行，直到文件的最后一行 
                    string tmp = sr.ReadLine();

                    while (tmp != null)
                    {
                        tmp = PathWrap.GetFullPathName(tmp);
                        if (list.Contains(tmp))
                        {
                            tmp = sr.ReadLine();
                            continue;
                        }
                        list.Add(tmp);
                        tmp = sr.ReadLine();
                    }
                    //关闭
                    sr.Close();
                    fs.Close();
                    return list;
                }
                catch (Exception e)
                {
                    return null;
                }
            }
        }
    }

}
