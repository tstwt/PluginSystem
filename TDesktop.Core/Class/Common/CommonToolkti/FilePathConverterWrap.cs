﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class FilePathConverterWrap
        {
            internal static String GetFileName(String filePath)
            {
                return Path.GetFileName(filePath);
            }

            internal static String GetRecentFilePath(String filePath)
            {
                String result = filePath.Replace("/", " > ");
                result = result.Replace("\\", " > ");
                return result;
            }
        }
    }
}
