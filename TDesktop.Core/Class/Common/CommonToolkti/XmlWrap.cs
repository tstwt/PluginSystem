﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class XmlWrap
        {
            /// <summary>
            /// 获取指定XML文档的根元素。
            /// </summary>
            internal static XmlNode GetRootNode(String xmlFile)
            {
                XmlNode rootNode = null;
                try
                {
                    XmlDocument xml = new XmlDocument();

                    String fileName = CommonToolkit.PathWrap.GetFullPathName(xmlFile);
                    xml.Load(fileName);
                    //第一个节点是默认的头，第二个结点就是要的
                    rootNode = xml.ChildNodes[1];
                }
                catch
                {
                    rootNode = null;
                }
                return rootNode;
            }

            /// <summary>
            /// 获取指定XML文档的根元素。
            /// </summary>
            internal static XmlNode GetRootNode(Stream inStream)
            {
                XmlNode rootNode = null;
                try
                {
                    XmlDocument xml = new XmlDocument();

                    xml.Load(inStream);
                    //第一个节点是默认的头，第二个结点就是要的
                    rootNode = xml.ChildNodes[1];
                }
                catch
                {
                    rootNode = null;
                }
                return rootNode;
            }

            /// <summary>
            /// 将XML内容写到指定路径文件。
            /// </summary>
            internal static void WriteToXmlFile(String strXml, String filePath)
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(strXml);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                FileStream file = new FileStream(filePath, FileMode.OpenOrCreate);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.OmitXmlDeclaration = true;
                settings.NewLineOnAttributes = true;
                settings.Encoding = Encoding.UTF8;
                settings.ConformanceLevel = ConformanceLevel.Fragment;
                settings.OmitXmlDeclaration = true;
                XmlWriter xmlWriter = XmlWriter.Create(file, settings);
                document.WriteTo(xmlWriter);
                file.Flush();
                xmlWriter.Flush();
                xmlWriter.Close();
                file.Close();
            }

            internal static XmlDocument GetXmlDocument(String prefix, String uri, String xml)
            {
                XmlDocument document = null;

                if (!String.IsNullOrWhiteSpace(xml))
                {
                    document = new XmlDocument();
                    XmlReader xmlReader = null;
                    NameTable nameTable = new NameTable();
                    XmlNamespaceManager xmlNamespaceMng = new XmlNamespaceManager(nameTable);
                    xmlNamespaceMng.AddNamespace(prefix, uri);
                    XmlParserContext parserContext = new XmlParserContext(null, xmlNamespaceMng, null, XmlSpace.None);
                    XmlReaderSettings settings = new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Auto };

                    Byte[] xmlBytes = Encoding.UTF8.GetBytes(xml);
                    using (MemoryStream stream = new MemoryStream(xmlBytes))
                    {
                        xmlReader = XmlReader.Create(stream, settings, parserContext);
                        document.Load(xmlReader);
                    }
                }

                return document;
            }

            internal static String GetSuperMapXmlValue(String nodeName, String attribute)
            {
                String value = null;
                try
                {
                    String xmlFile = PathWrap.GetFullPathName("SuperMap.xml");
                    XmlDocument xml = new XmlDocument();
                    xml.Load(xmlFile);
                    XmlNode rootNode = xml.ChildNodes[1];

                    foreach (XmlNode node in rootNode.ChildNodes)
                    {
                        if (node.LocalName.Equals(nodeName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            foreach (XmlNode child in node.ChildNodes)
                            {
                                if (child.LocalName.Equals(attribute, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    value = child.FirstChild.Value.ToString();
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
                return value;
            }

            internal static void SetSuperMapXmlValue(String nodeName, String attribute, String value)
            {
                try
                {
                    String xmlFile = PathWrap.GetFullPathName("SuperMap.xml");
                    XmlDocument xml = new XmlDocument();
                    xml.Load(xmlFile);
                    XmlNode rootNode = xml.ChildNodes[1];

                    foreach (XmlNode node in rootNode.ChildNodes)
                    {
                        if (node.LocalName.Equals(nodeName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            foreach (XmlNode child in node.ChildNodes)
                            {
                                if (child.LocalName.Equals(attribute, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    child.FirstChild.Value = value;
                                    break;
                                }
                            }
                        }
                    }
                    xml.Save(xmlFile);
                }
                catch (System.Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
            }
        }
    }
}
