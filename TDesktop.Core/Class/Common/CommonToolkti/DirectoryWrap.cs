﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class DirectoryWrap
        {
            internal static Int64 GetDirectorySize(String directoryPath)
            {
                Int64 size = 0;
                if (!String.IsNullOrEmpty(directoryPath)
                    && Directory.Exists(directoryPath))
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(directoryPath);
                    size = GetDirectorySize(directoryInfo);
                }

                return size;
            }

            internal static Int64 GetDirectorySize(DirectoryInfo directoryInfo)
            {
                Int64 size = 0;
                if (directoryInfo != null
                    && directoryInfo.Exists)
                {
                    DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories();
                    for (Int32 i = 0; i < directoryInfos.Length; i++)
                    {
                        size += GetDirectorySize(directoryInfos[i]);
                    }
                    FileInfo[] fileInfos = directoryInfo.GetFiles();
                    for (Int32 i = 0; i < fileInfos.Length; i++)
                    {
                        size += fileInfos[i].Length;
                    }
                }

                return size;
            }

            //将sourceDirectory下的所有文件以及文件夹复制到targetDirectory处
            internal static Boolean Copy(String sourceDirectory, String targetDirectory)
            {
                Boolean result = false;
                try
                {
                    if (!String.IsNullOrEmpty(sourceDirectory)
                        && !String.IsNullOrEmpty(targetDirectory)
                        && Directory.Exists(sourceDirectory))
                    {
                        DirectoryInfo targetInfo = new DirectoryInfo(targetDirectory);
                        if (!targetInfo.Exists)
                        {
                            targetInfo.Create();
                        }

                        String[] directorys = Directory.GetDirectories(sourceDirectory);
                        for (Int32 i = 0; i < directorys.Length; i++)
                        {
                            String directory = directorys[i];
                            DirectoryInfo directoryInfo = new DirectoryInfo(directory);
                            if ((directoryInfo.Attributes & FileAttributes.System) != FileAttributes.System
                                && (directoryInfo.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                            {
                                String subName = directoryInfo.Name;
                                DirectoryInfo targetSubInfo = targetInfo.CreateSubdirectory(subName);
                                if (!CommonToolkit.DirectoryWrap.Copy(directoryInfo.FullName, targetSubInfo.FullName))
                                {
                                    return false;
                                }
                            }
                        }
                        String[] files = Directory.GetFiles(sourceDirectory);
                        for (Int32 i = 0; i < files.Length; i++)
                        {
                            String sourceFile = files[i];
                            String fileName = Path.GetFileName(sourceFile);
                            String targetFileName = targetDirectory + Path.DirectorySeparatorChar + fileName;
                            File.Copy(sourceFile, targetFileName, true);
                        }
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.StackTrace, InfoType.Exception);
                }

                return result;
            }

            internal static String GetDirectoryName(String fullDirectoryName)
            {
                String directoryName = String.Empty;
                if (!String.IsNullOrEmpty(fullDirectoryName))
                {
                    try
                    {
                        DirectoryInfo directoryInfo = new DirectoryInfo(fullDirectoryName);
                        directoryName = directoryInfo.Name;
                    }
                    catch (System.Exception ex)
                    {
                        throw ex;
                    }
                }

                return directoryName;
            }
        }
    }
}
