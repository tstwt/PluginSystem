﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class ThreadWrap
        {
            internal static Thread CreateThread(ThreadStart threadStart)
            {
                try
                {
                    Thread thread = new Thread(threadStart);
                    try
                    {
                        //以下2个值必须保持一致，不然会出现错误，比如 土耳其+en 的组合，原因不在此详述
                        //设置当前线程的语言文化环境，用以正确处理字符串、数值转换，字符串比较等
                        thread.CurrentCulture = CultureInfo.GetCultureInfo("zh-CN");
                        //设置应用程序显示区域语言，用以加载对应的资源文件
                        thread.CurrentUICulture = CultureInfo.GetCultureInfo("zh-CN");
                    }
                    catch
                    {
                        thread.CurrentCulture = CultureInfo.GetCultureInfo("zh-CN");
                        thread.CurrentUICulture = CultureInfo.GetCultureInfo("zh-CN");
                    }

                    return thread;
                }
                catch (Exception ex)
                {
                    return null;
                    throw ex;
                }
            }

            internal static Thread CreateThread(ParameterizedThreadStart threadStart)
            {
                try
                {
                    Thread thread = new Thread(threadStart);
                    try
                    {
                        //以下2个值必须保持一致，不然会出现错误，比如 土耳其+en 的组合，原因不在此详述
                        //设置当前线程的语言文化环境，用以正确处理字符串、数值转换，字符串比较等
                        thread.CurrentCulture = CultureInfo.GetCultureInfo("zh-CN");
                        //设置应用程序显示区域语言，用以加载对应的资源文件
                        thread.CurrentUICulture = CultureInfo.GetCultureInfo("zh-CN");
                    }
                    catch
                    {
                        thread.CurrentCulture = CultureInfo.GetCultureInfo("zh-CN");
                        thread.CurrentUICulture = CultureInfo.GetCultureInfo("zh-CN");
                    }

                    return thread;
                }
                catch (Exception ex)
                {
                    return null;
                    throw ex;
                }
            }
        }
    }
}
