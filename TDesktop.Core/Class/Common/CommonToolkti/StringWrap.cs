﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDesktop
{
    internal partial class CommonToolkit
    {
        internal static class StringWrap
        {
            /// <summary>
            /// 取不超过指定字节数的字符串
            /// </summary>
            /// <param name="strSource">原字符串</param>
            /// <param name="byteCount">字节数</param>
            internal static String SubString(String strSource, Int32 byteCount)
            {
                Int32 intLength = 0;
                char[] aryLenB = strSource.ToCharArray();
                StringBuilder list = new StringBuilder();
                foreach (char chrLenB in aryLenB)
                {
                    if ((Int32)chrLenB > 255)
                    {
                        intLength += 2;
                    }
                    else
                    {
                        intLength++;
                    }

                    if (intLength <= byteCount)
                    {
                        list.Append(chrLenB);
                    }
                }
                return list.ToString();
            }

            /// <summary>
            /// 为可输入控件提供过滤非法字符的功能，之后把代码换成正则表达式 [6/5/2012 wuxb]
            /// </summary>
            /// <param name="sourceText">源文本</param>
            /// <param name="illegalChars">通用非法字符</param>
            /// <param name="startIllegalChars">用以过滤字符串首的非法字符集</param>
            /// <param name="endIllegalChars">用以过滤字符串末的非法字符集</param>
            /// <returns></returns>
            internal static String FilterText(String sourceText, Char[] illegalChars, Char[] startIllegalChars, Char[] endIllegalChars)
            {
                String text = sourceText;
                try
                {
                    if (illegalChars != null && illegalChars.Length > 0)
                    {
                        for (Int32 i = 0; i < illegalChars.Length; i++)
                        {
                            Int32 index = text.IndexOf(illegalChars[i]);
                            if (index > -1)
                            {
                                text = text.Remove(index, 1);
                            }
                        }
                    }

                    if (startIllegalChars != null && startIllegalChars.Length > 0)
                    {
                        for (Int32 i = 0; i < startIllegalChars.Length; i++)
                        {
                            Int32 index = text.IndexOf(startIllegalChars[i]);
                            if (index == 0)
                            {
                                text = text.Remove(index, 1);
                            }
                        }
                    }

                    if (endIllegalChars != null && endIllegalChars.Length > 0)
                    {
                        for (Int32 i = 0; i < endIllegalChars.Length; i++)
                        {
                            Int32 index = text.IndexOf(endIllegalChars[i]);
                            if (index == text.Length - 1)
                            {
                                text = text.Remove(index, 1);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
                return text;
            }

            /// <summary>
            /// 字符串是否合法
            /// </summary>
            /// <param name="name">要判断的名字</param>
            /// <param name="bAllowSpace">是否允许空格</param>
            /// <returns></returns>
            internal static Boolean IsLawName(String name, Boolean bAllowSpace)
            {
                Boolean isLaw = true;
                try
                {
                    List<Char> lawlessCode = new List<Char>();
                    lawlessCode.Add('/');
                    lawlessCode.Add('\\');
                    lawlessCode.Add('"');
                    lawlessCode.Add(':');
                    lawlessCode.Add('?');
                    lawlessCode.Add('*');
                    lawlessCode.Add('<');
                    lawlessCode.Add('>');
                    lawlessCode.Add('|');

                    if (!bAllowSpace)
                    {
                        name = name.TrimStart();
                    }
                    isLaw = IsLawName(name, lawlessCode.ToArray());
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
                return isLaw;
            }

            /// <summary>
            /// 字符串是否合法
            /// </summary>
            /// <param name="name">要判断的名字</param>
            /// <param name="illegalChars">非法字符集合</param>
            /// <returns></returns>
            internal static Boolean IsLawName(String name, Char[] illegalChars)
            {
                Boolean isLaw = true;
                try
                {
                    if (name.Length > 0)
                    {
                        for (Int32 i = 0; i < illegalChars.Length; i++)
                        {
                            if (name.Contains(illegalChars[i].ToString()))
                            {
                                isLaw = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        isLaw = false;
                    }
                }
                catch (Exception ex)
                {
                    Application.ActiveApplication.Output.Output(ex.Message, InfoType.Exception);
                }
                return isLaw;
            }
        }
    }
}
