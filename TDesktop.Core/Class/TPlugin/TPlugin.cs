﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop
{
    public abstract class TPlugin : IPlugin
    {
        private TPluginInfo m_pluginInfo;

        public void GetPluginInfo(TPluginInfo info)
        {
            m_pluginInfo = info;
        }

        /// <summary>
        /// 获取插件信息
        /// </summary>
        public TPluginInfo PluginInfo { get => m_pluginInfo; }

        /// <summary>
        /// 插件构造
        /// </summary>
        /// <returns></returns>
        public abstract TDockBar Initialize();
    }
}
