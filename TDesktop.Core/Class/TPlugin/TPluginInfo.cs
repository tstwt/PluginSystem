﻿/**************************************************************** 
 * 作    者：TangWeitian 
 * CLR 版本：4.0.30319.42000 
 * 创建时间：2017/6/27 13:48:00 
 * 当前版本：
 * 描述说明： 
 * 
 * 修改历史： 
 * 
***************************************************************** 
 * Copyright @ TangWeitian 2017 All rights reserved 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDesktop
{
    public class TPluginInfo
    {
        private String m_name;
        private String m_filePath;
        private String m_Version;
        /// <summary>
        /// 插件名称
        /// </summary>
        public String Name { get => m_name; set => m_name = value; }
        /// <summary>
        /// 插件路径
        /// </summary>
        public String FilePath { get => m_filePath; set => m_filePath = value; }
        /// <summary>
        /// 插件版本
        /// </summary>
        public String Version { get => m_Version; set => m_Version = value; }

        internal TPluginInfo()
        {
            m_name = String.Empty;
            m_filePath = String.Empty;
            m_Version = String.Empty;
        }
    }
}
