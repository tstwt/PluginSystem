﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Docking2010.Views.Tabbed;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using TDesktop.Controls;

namespace TDesktop
{
    internal partial class MainFrm : DevExpress.XtraEditors.XtraForm, IMainForm
    {
        public MainFrm(IPlugin[] Tplugins)
        {
            //触发界面加载事件
            if (Loading != null)
            {
                Loading(this, new EventArgs());
            }

            LoadUI(Tplugins);

            //触发界面加载后之间
            if (Loaded != null)
            {
                Loaded(this, new EventArgs());
            }
        }

        #region 属性

        /// <summary>
        /// 修改主题
        /// </summary>
        public ThemeStyle TStyle
        {
            get
            {
                switch (this.Theme.LookAndFeel.SkinName)
                {
                    case "Office 2016 Colorful":
                        return ThemeStyle.Office2016Colorful;

                    case "Visual Studio 2013 Dark":
                        return ThemeStyle.VisualStudio2013Dark;

                    case "Office 2010 Blue":
                        return ThemeStyle.Office2010Blue;

                    case "DevExpress Style":
                        return ThemeStyle.DevExpressStyle;
                    default:
                        return ThemeStyle.Office2016Colorful;
                }
            }

            set
            {
                switch (value)
                {
                    case ThemeStyle.Office2016Colorful:
                        this.Theme.LookAndFeel.SkinName = "Office 2016 Colorful";
                        break;

                    case ThemeStyle.VisualStudio2013Dark:
                        this.Theme.LookAndFeel.SkinName = "Visual Studio 2013 Dark";
                        break;

                    case ThemeStyle.Office2010Blue:
                        this.Theme.LookAndFeel.SkinName = "Office 2010 Blue";
                        break;

                    case ThemeStyle.DevExpressStyle:
                        this.Theme.LookAndFeel.SkinName = "DevExpress Style";
                        break;
                }

                ThemeStyleChangedEventArgs args = new ThemeStyleChangedEventArgs(value);
                if (ThemeStyleChanged != null)
                {
                    ThemeStyleChanged(this.Theme, args);
                }
            }
        }

        #endregion

        #region 内部方法

        private void LoadUI(IPlugin[] Tplugins)
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();//应用皮肤
            DevExpress.XtraEditors.Controls.Localizer.Active = new DevExpressCHS();//修改messagebox按钮文字

            InitializeComponent();

            if (Tplugins.Length == 1)
            {
                TDockBar dock = Tplugins[0].Initialize();
                this.Controls.Add(dock.Control);
                this.Text = dock.Caption;
            }
            else
            {
                List<DockPanel> dockbars = new List<DockPanel>();
                foreach (IPlugin plugin in Tplugins)
                {
                    TDockBar control = plugin.Initialize();
                    if (control != null)
                    {
                        dockbars.Add(control.DockPanel);
                    }
                }
                dockManager1.RootPanels.AddRange(dockbars.ToArray());
            }
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            //标签放到第一个
            tabbedView1.Controller.Activate(documentManager1.View.Documents[0]);
        }

        #endregion

        #region 公共方法

        /// <summary>
        /// 在主界面实现插件
        /// </summary>
        /// <param name="plugins"></param>
        internal void AddDockPanel(IPlugin[] plugins)
        {
            foreach (IPlugin plugin in plugins)
            {
                TDockBar control = plugin.Initialize();
                if (control != null)
                {
                    //control.DockPanel.Register(dockManager1);
                    DockPanel newDockPanel = dockManager1.AddPanel(DockingStyle.Left);
                    newDockPanel.ControlContainer.Controls.Add(control.DockPanel.ControlContainer.Controls[0]);
                    newDockPanel.Text = control.Caption;
                    newDockPanel.DockedAsTabbedDocument = control.DockedAsTab;
                }
            }
        }

        private void MainFrm_Resize(object sender, EventArgs e)
        {
            SizeChangingEventArgs args = new SizeChangingEventArgs(this.Size);
            if (SizeChanging != null)
            {
                SizeChanging(this, args);
            }
        }

        private void MainFrm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CancelEventArgs args = new CancelEventArgs(false);
            if (MainClosing != null)
            {
                MainClosing(this, args);
            }
            e.Cancel = args.Cancel;
        }

        private void MainFrm_FormClosed(object sender, FormClosedEventArgs e)
        {

            if (MainClosed != null)
            {
                MainClosed(this, new EventArgs());
            }
        }

        #endregion

        #region Event_Number

        /// <summary>
        /// 加载主窗口界面元素后将触发该事件。 
        /// </summary>
        public event EventHandler Loaded;

        /// <summary>
        /// 加载主窗口界面元素时将触发该事件。
        /// </summary>
        public event EventHandler Loading;

        /// <summary>
        /// 变更主题后将触发该事件
        /// </summary>
        public event ThemeStyleChangedEventHandler ThemeStyleChanged;

        /// <summary>
        /// 大小变更的时候触发的事件
        /// </summary>
        public event SizeChangingEventHandler SizeChanging;

        /// <summary>
        /// 窗口关闭时将触发的事件
        /// </summary>
        public event EventHandler<CancelEventArgs> MainClosing;

        /// <summary>
        /// 窗口关闭后触发的事件
        /// </summary>
        public event EventHandler MainClosed;

        #endregion
    }
}
