﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraRichEdit.API.Word;
using Application = System.Windows.Forms.Application;

namespace TDesktop.Communication
{
    class Unity
    {
        private static string projectPath = Application.StartupPath + "/Data/Project/Project.xml";
        private static string childProjectPath = Application.StartupPath + "/Data/ChlidProject/{0}.xml";
        /// <summary>
        /// 项目文件地址
        /// </summary>
        public static string ProjectPath
        {
            get { return projectPath; }
        }
        /// <summary>
        /// 子项目文件地址
        /// </summary>
        public static string ChildProjectPath
        {
            get { return childProjectPath; }
        }
        /// <summary>
        /// 根据Xml返回Table
        /// </summary>
        /// <param name="filePath">Xml路径</param>
        /// <returns>DataTable</returns>
        public static DataTable GetProjectFromXml(string filePath)
        {
            if (!File.Exists(filePath))
            {
                if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                }
                return InitTable(filePath, ProjectType.ProjectList);
            }

            DataTable table = new DataTable();
            try
            {
                table.ReadXml(filePath);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return null;
            }
            return table;
        }

        /// <summary>
        /// 从XMl返回子项目Table
        /// </summary>
        /// <param name="filePath">Xml路径</param>
        /// <returns>DataTable</returns>
        public static DataTable GetChildProjectFromXml(string filePath)
        {
            if (!File.Exists(filePath))
            {
                if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath));
                }
                return InitTable(filePath, ProjectType.ChildProject);
            }

            DataTable table = new DataTable();
            try
            {
                table.ReadXml(filePath);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return null;
            }
            return table;
        }

        /// <summary>
        /// 初始化Datatable
        /// </summary>
        /// <param name="filepath">Xml路径</param>
        /// <returns></returns>
        private static DataTable InitTable(string filepath, ProjectType type)
        {
            DataTable table = new DataTable();
            if (type == ProjectType.ProjectList)
            {

                //table.Namespace = "项目目录";
                table.TableName = "项目名称";
                table.Columns.Add("项目名称", typeof(string));
                //table.Rows.Add(new object[] {"你好"});
                table.WriteXml(filepath, XmlWriteMode.WriteSchema);
                return table;
            }
            else
            {
                table.TableName = Path.GetFileNameWithoutExtension(filepath);
                table.Columns.Add("日期", typeof(string));
                table.Columns.Add("问题来源", typeof(string));
                table.Columns.Add("问题简述", typeof(string));
                table.Columns.Add("是否解决", typeof(string));
                table.Columns.Add("解决方案", typeof(string));
                table.Columns.Add("解决时间", typeof(string));
                table.WriteXml(filepath, XmlWriteMode.WriteSchema);
                return table;
            }
        }

        /// <summary>
        /// 获得所有问题来源
        /// </summary>
        /// <param name="gridView"></param>
        /// <returns></returns>
        public static List<string> GetAllQuestionFrom(GridView gridView)
        {
            List<string> QuestionList = new List<string>();
            for (int i = 0; i < gridView.RowCount; i++)
            {
                string temp = gridView.GetRowCellDisplayText(i, "问题来源");
                Boolean isHave = false;
                //判断是否已经存在于list
                for (int j = 0; j < QuestionList.Count; j++)
                {
                    if (QuestionList[j] == temp)
                    {
                        isHave = true;
                    }
                }

                if (!isHave)
                {
                    QuestionList.Add(temp);
                }
            }

            return QuestionList;
        }

        /// <summary>
        /// 获得当前行所有数据
        /// </summary>
        /// <param name="index"></param>
        /// <param name="gridView1"></param>
        /// <returns></returns>
        public static List<string> GetAllData(int index, GridView gridView1)
        {
            List<string> temp = new List<string>();
            //日期
            string StartTime = gridView1.GetRowCellValue(index, "日期").ToString();
            //问题来源
            string Question_From = gridView1.GetRowCellValue(index, "问题来源").ToString();
            //问题简述
            string Question_Info = gridView1.GetRowCellDisplayText(index, "问题简述");
            //是否解决
            string isOver = gridView1.GetRowCellDisplayText(index, "是否解决");
            //如何解决
            string solution = gridView1.GetRowCellDisplayText(index, "解决方案");
            //解决时间
            string OverTime = gridView1.GetRowCellValue(index, "解决时间").ToString();
            temp.AddRange(new string[]
            {
                StartTime,
                Question_From,
                Question_Info,
                isOver,
                solution,
                OverTime
            });

            return temp;
        }

        /// <summary>
        /// 设置当前行所有数据
        /// </summary>
        /// <param name="index"></param>
        /// <param name="gridView1"></param>
        public static void SetAllData(int index, List<string> list, GridView gridView1)
        {
            //日期
            gridView1.SetRowCellValue(index, "日期", list[0]);
            //问题来源
            gridView1.SetRowCellValue(index, "问题来源", list[1]);
            //问题简述
            gridView1.SetRowCellValue(index, "问题简述", list[2]);
            //是否解决
            gridView1.SetRowCellValue(index, "是否解决", list[3]);
            //如何解决
            gridView1.SetRowCellValue(index, "解决方案", list[4]);
            //解决时间
            gridView1.SetRowCellValue(index, "解决时间", list[5]);
        }

        /// <summary>
        /// 单元格换行设置
        /// </summary>
        /// <param name="gridView1"></param>
        public static void SetColumnEdit(GridView gridView1)
        {
            DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit x =
                new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            //gridView1.Columns[2].ColumnEdit = x;
            //gridView1.Columns[4].ColumnEdit = x;
            //gridView1.Columns[2].AppearanceCell.TextOptions.WordWrap = WordWrap.Wrap;
            //gridView1.Columns[4].AppearanceCell.TextOptions.WordWrap = WordWrap.Wrap;
            foreach (GridColumn column in gridView1.Columns)
            {
                column.AppearanceCell.Font = new System.Drawing.Font("微软雅黑", 10);
                column.ColumnEdit = x;
                column.AppearanceCell.TextOptions.VAlignment = VertAlignment.Center;
                column.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                column.AppearanceCell.TextOptions.WordWrap = WordWrap.Wrap;
                column.AppearanceHeader.Font = new System.Drawing.Font("微软雅黑", 9);
            }
        }

        /// <summary>
        /// Project类型
        /// </summary>
        enum ProjectType
        {
            /// <summary>
            /// 主项目
            /// </summary>
            ProjectList,
            /// <summary>
            /// 子项目
            /// </summary>
            ChildProject
        }
    }
}
