﻿using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace TDesktop.Communication
{
    internal partial class ProjectInfo : XtraForm
    {
        private DataTable table;
        private Point point;
        private static ProjectInfo frm = null;

        public static ProjectInfo GetFrm(DataTable _table, Point _point)
        {
            if (frm != null)
            {
                frm.Close();
            }
            frm = new ProjectInfo(_table, _point);
            return frm;
        }

        private ProjectInfo(DataTable _table, Point _point)
        {
            InitializeComponent();

            table = _table;
            point = _point;
        }

        private void Form2_Load(object sender, System.EventArgs e)
        {
            this.Location = point;
            gridControl1.DataSource = table;
        }

        private void Form2_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            table.WriteXml(Unity.ProjectPath, XmlWriteMode.WriteSchema);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton2_Click(object sender, System.EventArgs e)
        {
            int index = gridView1.GetSelectedRows()[0];
            string name = gridView1.GetRowCellDisplayText(index, "项目名称");
            if (XtraMessageBox.Show(this, "确定删除\"" + name + "\"？", "问题", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                return;
            }
            gridView1.DeleteRow(index);
            File.Delete(string.Format(Unity.ChildProjectPath, name));
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton1_Click(object sender, System.EventArgs e)
        {
            if (textBox1.Text == "")
            {
                XtraMessageBox.Show(this, "名称不能为空", "提示");
                return;
            }
            table.Rows.Add(new object[] { textBox1.Text });
        }
    }
}
