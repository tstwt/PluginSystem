﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TDesktop.Controls;

namespace TDesktop.Communication
{
    public class Communication : TPlugin
    {
        public override TDockBar Initialize()
        {
            TDockBar dock = new TDockBar();
            dock.Caption = "用户沟通记录";
            dock.Control = new MainControl();
            return dock;
        }
    }
}
