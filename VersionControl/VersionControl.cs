﻿/**************************************************************** 
 * 作    者：TangWeitian 
 * CLR 版本：4.0.30319.42000 
 * 创建时间：2017/6/27 17:04:31 
 * 当前版本：
 * 描述说明： 
 * 
 * 修改历史： 
 * 
***************************************************************** 
 * Copyright @ TangWeitian 2017 All rights reserved 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraBars.Docking;
using TDesktop;
using TDesktop.Controls;

namespace TDesktop
{
    class Versioner : TPlugin
    {
        public override TDockBar Initialize()
        {
            TDockBar dock = new TDockBar();
            dock.DockedAsTab = false;
            dock.Caption = "版本控制器";
            dock.DockStyle = DockingStyle.Right;
            dock.Control = new TDesktop.VersionControl.Version();
            return dock;
        }
    }
}
